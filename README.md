# README

### Summary

* Front end files for the demand-side server (demand.team24.softwareengineeringii.com).

### Navigation

* `index.css`
	* Styling for HTML pages.
* `register.html`
    * Page where customers can create an account.
* `login.html`
    * Page where customers log in.
* `order.html`
    * Page where customers can place an order.

### Team members

* TM1 - Sydney Mack
* TM2 - Mariah Sager
* TM3 - Keldon Boswell
* TM4 - Colby Hayes
* TM5 - Karla Salto
